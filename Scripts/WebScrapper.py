### Libraries

import requests
from bs4 import BeautifulSoup
import pandas

### Parameters

URL = "https://www.ebay.com/sch/i.html?_from=R40&_nkw=christmas+gift&_sacat=0&_ipg=200"

### Functions

def GetHTML(URL): # Generic function to get an html page
    # Used headers/agent because the request was timed out and asking for an agent.
    # Using following code we can fake the agent.
    headers = {'User-Agent': 'Mozilla/5.0'}
    response = requests.get(URL, headers=headers)

    content = response.content
    soup = BeautifulSoup(content,"html.parser")
    return soup

### Main Program

soup = GetHTML(URL) # Getting the HTML page

#### Treating specifically the ebay results HTML page

top_articles = soup.find_all("div",attrs={"class": "s-item__wrapper clearfix"})
top_articles = top_articles[1:]

list_articles =[]
for article in top_articles: # Going through the articles
    dataframe ={}
    dataframe["article_name"] = (article.find("h3",attrs={"class": "s-item__title"})).text.replace('\n', ' ')
    balise_img = article.find("img",attrs={"class": "s-item__image-img"})  
    if balise_img:
        dataframe["article_image"] = str(balise_img)[str(balise_img).find('src')+6:-4]
    balise_price = article.find("span",attrs={"class": "s-item__price"})
    if balise_price:
        dataframe["item_price"] = balise_price.text
    list_articles.append(dataframe)

df = pandas.DataFrame(list_articles)
df.to_csv("../Results/EbayChristmasGifts.csv",index=False) # Saving the results to be able to exploit it later
