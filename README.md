# WebScrapping

This is a small personnal projet to get a kind of reminder of the basics of web data scrapping and data cleaning. Here, the example of the website Ebay is taken, to get the results of the request *Chrismas gifts*.
![](./Images/ScreenshotEbayResults.png)

## Functioning

The python script `WebScrapper.py` can get the HTML code of a website. The only thing to give him is the URL of the web page. This is done with the `GetHTML()` function. The second part of the script is treating more precisely the example here: the christmas gifts answers of ebay. It is dependant of the website's structure, so the code cannot be made general.

The R script `CheckingScrapping.r` is just here to be able to easily visualize the data frame obtained.

## Usage

Just run the python script to get the dataframe. Run the R script to see the dataframe.

## Acknowledgment

This project is based on the tutorial given here : https://datascienceplus.com/zomato-web-scraping-with-beautifulsoup-in-python/

## Development

This project is ended and no longer in development.
